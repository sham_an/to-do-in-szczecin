### To do in Szczecin
Simple app where you can mark places and things that you have seen or done in Szczecin. The main goal of creating this project was learning how to use a flexbox.
This project will be further developed with new elements. Background image was made by me using Adobe Photoshop software. 
#### Used technologies:
* HTML5
* CSS3
* JavaScript
Here you can see live version -> http://todoszczecin.frontapp.webd.pro/