const addOverlay = () => {
    const overlay =
        `<div class="opacity-font">
            <div class="inner">
                <i class="fas fa-check-circle large"></i>
            </div>
        </div>`;

    const overlayBoxes = document.querySelectorAll('.overlay-box');

    overlayBoxes.forEach((overlayBox) => {
        overlayBox.addEventListener('click', () => {
          if (overlayBox.innerHTML === "") {
            overlayBox.innerHTML = overlay;
          } else {
            overlayBox.innerHTML = "";
          }
        });
    });
}

addOverlay();
